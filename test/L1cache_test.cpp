/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>



class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */  
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
    /* Force a miss  */
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for (int j =  0; j < associativity; j++) {
      cache_line[j].valid = true;
      cache_line[j].tag = rand()%4096;
      cache_line[j].dirty = 0;
      cache_line[j].rp_value = j;
      while (cache_line[j].tag == tag) {
        cache_line[j].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  } 
}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  struct operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  struct entry cache_line[associativity];
  /* Check for a miss */
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for (int j =  0; j < associativity; j++) {
      cache_line[j].valid = true;
      cache_line[j].tag = rand()%4096;
      cache_line[j].dirty = 0;
      cache_line[j].rp_value = 1;
      while (cache_line[j].tag == tag) {
        cache_line[j].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = nru_replacement_policy( idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  } 
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){
  // int policy;
  // int status;
  // int i;
  // int idx;
  // int tag;
  // int associativity;
  // int N;
  // enum miss_hit_status expected_miss_hit;
  // bool loadstore = bool(rand() % 2);
  // bool debug = 1;
  // struct operation_result result = {};

  // /*Choose a random policy */
  // policy = rand() % 3;
  // /* Fill a random cache entry */
  // idx = rand() % 1024;
  // tag = rand() % 4096;
  // /* Choose a random associativity */
  // associativity = 1 << (rand() % 4);
  // if (debug_on) {
  //   printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n", idx, tag,
  //          associativity);
  // }

  // struct entry cache_line[associativity];
  // /* Fill cache line */
  // for (int j = 0; j < associativity; j++) {
  //   cache_line[j].valid = true;
  //   cache_line[j].tag = rand() % 4096;
  //   cache_line[j].dirty = 0;
  //   while (cache_line[j].tag == tag) {
  //     cache_line[j].tag = rand() % 4096;
  //   }
  // }
  // /* Force a hit on A */
  // int Block_A_tag;
  // int Block_A_index;
  // Block_A_tag = cache_line[0].tag;
  // Block_A_index = 0;
  
  // if (policy == 0) {
  //   for (int j = 0; j < associativity; j++) {
  //     if(associativity <= 2){
  //       cache_line[j].rp_value = 0;
  //     }else{
  //       cache_line[j].rp_value = 2;
  //     }
  //   }
  //   status = srrip_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                                 cache_line, &result, bool(debug_on));
  //   expected_miss_hit = loadstore ? HIT_STORE : HIT_LOAD;
  //   EXPECT_EQ(result.miss_hit, expected_miss_hit);

  // } else if (policy == 1) {
  //   for (int j = 0; j < associativity; j++) {
  //     cache_line[j].rp_value = j;
  //   }
  //   status = lru_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                               cache_line, &result, bool(debug_on));
  //   expected_miss_hit = loadstore ? HIT_STORE : HIT_LOAD;
  //   EXPECT_EQ(result.miss_hit, expected_miss_hit);

  // } else {
  //   for (int j = 0; j < associativity; j++) {
  //     cache_line[j].rp_value = 1;
  //   }
  //   status = nru_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                               cache_line, &result, bool(debug_on));
  //   expected_miss_hit = loadstore ? HIT_STORE : HIT_LOAD;
  //   EXPECT_EQ(result.miss_hit, expected_miss_hit);
  //   }

  // /*Check rp value*/
  // EXPECT_EQ(cache_line[Block_A_index].rp_value, 0);
  // N = 0;
  // /*Keep inserting new block until A is evicted*/
  // bool A_evicted = false;
  // while (!A_evicted) {
  //   int i = 0;
  //   tag = rand() % 4096;

  //   if (policy == 0) {
      
  //     status = srrip_replacement_policy(idx, tag, associativity, loadstore,
  //                                       cache_line, &result, bool(debug_on));
  //     if (result.evicted_address == Block_A_tag){
  //         A_evicted = true;
  //     }
      
  //   } else if (policy == 1) {
      
  //     status = lru_replacement_policy(idx, tag, associativity, loadstore,
  //                                     cache_line, &result, bool(debug_on));
  //     if (result.evicted_address == Block_A_tag){
  //         A_evicted = true;
  //     }
      
  //   } else {
      
  //     status = nru_replacement_policy(idx, tag, associativity, loadstore,
  //                                     cache_line, &result, bool(debug_on));
  //     if (result.evicted_address == Block_A_tag){
  //         A_evicted = true;
  //     }
  //   }
  //   N += 1;
  // }
  // /*Check eviction of block A happen after N new blocks were inserted*/
  // if (policy > 0){
  //   EXPECT_EQ(N, associativity);
  // }
}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
   //  int policy;
  //  int status;
  //  int i;
  //  int idx;
  //  int tag;
  //  int associativity;
  //  enum miss_hit_status expected_miss_hit;
  //  bool loadstore = 1;
  //  bool debug = 0;
  //  struct operation_result result = {};
  //  /*Choose a random policy */
  //  policy = 1;
  //  /* Choose a random associativity */
  //  associativity = 1 << (rand() % 4);
  //  /* Fill a random cache entry with only read operations */
  //  idx = rand() % 1024;
   
   
  //  struct entry cache_line[associativity];
  //    /* Fill cache line */
  //    for (int j = 0; j < associativity; j++) {
  //      cache_line[j].valid = false;
  //    }
       
  //    for (int j = 0; j < associativity; j++) {
  //       tag = j * 100 + 2;
  //       if (debug_on) {
  //         printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n", idx, tag,
  //                 associativity);
  //       }
      
  //       loadstore = 0;
  //       if (policy == 0) {
  //         status = srrip_replacement_policy(idx, tag, associativity, loadstore,
  //                                           cache_line, &result, bool(debug_on));
  //       } else if (policy == 1) {
  //         status = lru_replacement_policy(idx, tag, associativity, loadstore,
  //                                         cache_line, &result, bool(debug_on));
  //       } else {
  //         status = nru_replacement_policy(idx, tag, associativity, loadstore,
  //                                         cache_line, &result, bool(debug_on));
  //       }
  //    }
  //  /*Force a write hit for a random block A*/
  //  int Block_A_tag;
  //  int Block_A_index;
  //  Block_A_tag = cache_line[0].tag;
  //  Block_A_index = 0;
  //  loadstore = 1;
  //  if (policy == 0) {
  //    status = srrip_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                                      cache_line, &result, bool(debug_on));
  //  } else if (policy == 1) {
  //    status = lru_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                                    cache_line, &result, bool(debug_on));
  //  } else {
  //    status = nru_replacement_policy(idx, Block_A_tag, associativity, loadstore,
  //                                    cache_line, &result, bool(debug_on));
  //  }
  // EXPECT_EQ(cache_line[Block_A_index].dirty, true);
  //   /*Keep inserting new lines until A is evicted*/
  //  while (cache_line[Block_A_index].tag == Block_A_tag) {
  //    int i = 0;
  //    tag = rand() % 4096;
  //    if(tag!=Block_A_tag){
  //       if (policy == 0) {

  //           loadstore = rand() % 2;
  //           status = srrip_replacement_policy(idx, tag, associativity, loadstore,
  //                                             cache_line, &result, bool(debug_on));
          
  //       } else if (policy == 1) {
  //           loadstore = rand() % 2;
  //           status = lru_replacement_policy(idx, tag, associativity, loadstore,
  //                                           cache_line, &result, bool(debug_on));
          
  //       } else {
          
  //           loadstore = rand() % 2;
  //           status = nru_replacement_policy(idx, tag, associativity, loadstore,
  //                                           cache_line, &result, bool(debug_on));
          
  //       }
        
  //    }
  //  }

  //  /*Check dirty bit of block B is false */
  //  EXPECT_EQ(result.dirty_eviction, true); 

  //    /* Fill cache line */
  //    for (int j = 0; j < associativity; j++) {
  //      cache_line[j].valid = false;
  //    }
       
  //    for (int j = 0; j < associativity; j++) {
  //     tag = j * 100 + 2;
  //     if (debug_on) {
  //       printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n", idx, tag,
  //               associativity);
  //     }
     
  //     loadstore = 0;
  //     if (policy == 0) {
  //       status = srrip_replacement_policy(idx, tag, associativity, loadstore,
  //                                         cache_line, &result, bool(debug_on));
  //     } else if (policy == 1) {
  //       status = lru_replacement_policy(idx, tag, associativity, loadstore,
  //                                       cache_line, &result, bool(debug_on));
  //     } else {
  //       status = nru_replacement_policy(idx, tag, associativity, loadstore,
  //                                       cache_line, &result, bool(debug_on));
  //     }
  //    }

  //  /*Force a read hit for a random block B*/
  //  int Block_B_tag;
  //  int Block_B_index;
  //  Block_B_tag = cache_line[1].tag;
  //  Block_B_index = 1;
  //  loadstore = 0;
  //  if (policy == 0) {
  //    status = srrip_replacement_policy(idx, Block_B_tag, associativity, loadstore,
  //                                 cache_line, &result, bool(debug_on));
  //  } else if (policy == 1) {
  //    status = lru_replacement_policy(idx, Block_B_tag, associativity, loadstore,
  //                                    cache_line, &result, bool(debug_on));
  //  } else {
  //    status = nru_replacement_policy(idx, Block_B_tag, associativity, loadstore,
  //                                    cache_line, &result, bool(debug_on));
  //  }
  //   EXPECT_EQ(cache_line[Block_B_index].dirty, false);
  //  /*Keep inserting new lines until B is evicted*/
  //  while (cache_line[Block_B_index].tag == Block_B_tag) {
  //    int i = 0;
  //    tag = rand() % 4096;
  //    if(tag!=Block_B_tag){
  //       if (policy == 0) {
          
  //           loadstore = rand() % 2;
  //           status = srrip_replacement_policy(idx, tag, associativity, loadstore,
  //                                             cache_line, &result, bool(debug_on));
          
  //       } else if (policy == 1) {
          
  //           loadstore = rand() % 2;
  //           status = lru_replacement_policy(idx, tag, associativity, loadstore,
  //                                           cache_line, &result, bool(debug_on));
          
  //       } else {
          
  //           loadstore = (bool)i;
  //           status = nru_replacement_policy(idx, tag, associativity, loadstore,
  //                                           cache_line, &result, bool(debug_on));
          
  //       }
  //    }
  //  }
  //  /*Check dirty bit of block B is false */
  //  EXPECT_EQ(result.dirty_eviction, false);
}
/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and asociativy
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){
   int policy;
   int status;
   int i;
   int idx;
   int tag;
   int associativity;
   enum miss_hit_status expected_miss_hit;
   bool loadstore = 1;
   bool debug = 0;
   struct operation_result result = {};
   /*Choose a random policy*/
   policy = rand() % 3;

   /* Choose valid parameters for inx, tag and associativity*/
   idx = -4;
   tag = -1161;
   associativity = 0;

   struct entry cache_line[associativity];
   if (policy == 0) {
     status = srrip_replacement_policy(idx, tag, associativity, loadstore,
                                       cache_line, &result, true);
   } else if (policy == 1) {
     status = lru_replacement_policy(idx, tag, associativity, loadstore,
                                     cache_line, &result, true);
   } else {
     status = nru_replacement_policy(idx, tag, associativity, loadstore,
                                     cache_line, &result, true);
   }
   /*Checks a returns PARAM error condition*/
   EXPECT_EQ(status, 1);
}
