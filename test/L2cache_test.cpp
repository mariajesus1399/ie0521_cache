/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include "debug_utilities.h"
#include "L1cache.h"
#include "L2cache.h"

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: miss on L1 and hit on L2
 * Choose a random associativity,tag and index
 * Fill cache
 * Force L2 block hit
 * Force a L1 block miss
 * Check miss status L1 and hit for L2
 * Force L1 hit and L2 hit
 * Check hit status L1 and L2
 */
TEST_F(L2cache,p_l1_miss_l2_hit) {
  int status;
  struct operation_result l1_result = {};
  struct operation_result l2_result = {};
  enum miss_hit_status expected_miss_hit_l1;
  enum miss_hit_status expected_miss_hit_l2;
  u_int idx;
  u_int tag;
  u_int associativity_l1, associativity_l2;
  bool l1_full = false;
  srand(time(0));

  /* Choose a random associativity,tag and index */
  idx = rand() % 1024;
  tag = rand() % 4096;
  associativity_l1 = 1 << (rand() % 4);
	associativity_l2 = 2*associativity_l1;

  entry_info l1_l2_info = {0,tag,idx,0,associativity_l1,tag,idx,0,associativity_l2,0};

  struct entry l1_cache_blocks[associativity_l1];
  struct entry l2_cache_blocks[associativity_l2];

  /* Fill cache */
  int random_tag;
  for (int j = 0; j < associativity_l2 - 1; j++) {
    random_tag = rand() % 4096;

    while (random_tag == tag) {
      random_tag += 1;
    }
    if (!l1_full) {
      l1_cache_blocks[j].valid = true;
      l1_cache_blocks[j].tag = random_tag;
      l1_cache_blocks[j].dirty = 0;
      l1_cache_blocks[j].rp_value = j;
    }
    l2_cache_blocks[j].valid = true;
    l2_cache_blocks[j].tag = random_tag;
    l2_cache_blocks[j].dirty = 0;
    l2_cache_blocks[j].rp_value = j;

    if (j == associativity_l1) {
      l1_full = true;
    }
  }

  /* Force L2 hit */
  l2_cache_blocks[associativity_l2 - 1].valid = true;
  l2_cache_blocks[associativity_l2 - 1].tag = tag;
  l2_cache_blocks[associativity_l2 - 1].dirty = 0;
  l2_cache_blocks[associativity_l2 - 1].rp_value = associativity_l2 - 1;

	/* Force L1 miss */
  bool loadstore = bool(rand() % 2);
  status = lru_replacement_policy_l1_l2(&l1_l2_info, loadstore, l1_cache_blocks,
                                   l2_cache_blocks, &l1_result, &l2_result, 0);

	/* Check results */
  EXPECT_EQ(status, 0);
  EXPECT_EQ(l2_result.dirty_eviction, 0);
  expected_miss_hit_l1 = loadstore ? MISS_STORE : MISS_LOAD;
  EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
  expected_miss_hit_l2 = loadstore ? HIT_STORE : HIT_LOAD;
  EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);

	/* Force L1 and L2 hit */
  status = lru_replacement_policy_l1_l2(&l1_l2_info, loadstore, l1_cache_blocks,
                                   l2_cache_blocks, &l1_result, &l2_result, 0);
	
	/* Check results */
  EXPECT_EQ(status, 0);
  EXPECT_EQ(l2_result.dirty_eviction, 0);
  expected_miss_hit_l1 = loadstore ? HIT_STORE : HIT_LOAD;
  EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
  expected_miss_hit_l2 = loadstore ? HIT_STORE : HIT_LOAD;
  EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
}

/*
 * TEST2: miss on L1 and miss on L2
 * Choose a random associativity,tag and index
 * Fill cache
 * Force a L1 and L2 block miss
 * Check miss status L1 and hit for L2
 * Force L1 hit and L2 hit
 * Check hit status L1 and L2
 */
TEST_F(L2cache,p_l1_miss_l2_miss) {
  int status;
  struct operation_result l1_result = {};
  struct operation_result l2_result = {};
  enum miss_hit_status expected_miss_hit_l1;
  enum miss_hit_status expected_miss_hit_l2;
  int idx;
  int tag;
  int associativity_l1, associativity_l2;
  bool l1_full = false;
  srand(time(0));

	/* Choose a random associativity,tag and index */
  idx = rand() % 1024;
  tag = rand() % 4096;
  associativity_l1 = 1 << (rand() % 4);
  associativity_l2 = 2 * associativity_l1;

	entry_info l1_l2_info;
	l1_l2_info.l1_idx = idx;
	l1_l2_info.l1_tag = tag;
	l1_l2_info.l1_assoc = associativity_l1;
	l1_l2_info.l2_idx = idx;
	l1_l2_info.l2_tag = tag;
	l1_l2_info.l2_assoc = associativity_l1;
	

  struct entry l1_cache_blocks[associativity_l1];
  struct entry l2_cache_blocks[associativity_l2];

	/* Fill cache */
  int random_tag;
  for (int j = 0; j < associativity_l2; j++) {
    random_tag = rand() % 4096;

    while (random_tag == tag) {
      random_tag += 1;
    }
    if (!l1_full) {
      l1_cache_blocks[j].valid = true;
      l1_cache_blocks[j].tag = random_tag;
      l1_cache_blocks[j].dirty = 0;
      l1_cache_blocks[j].rp_value = j;
    }
    l2_cache_blocks[j].valid = true;
    l2_cache_blocks[j].tag = random_tag;
    l2_cache_blocks[j].dirty = 0;
    l2_cache_blocks[j].rp_value = j;

    if (j == associativity_l1) {
      l1_full = true;
    }
  }

	/* Force L1 and L2 miss */
  bool loadstore = bool(rand() % 2);
  status = lru_replacement_policy_l1_l2(&l1_l2_info, loadstore, l1_cache_blocks,
                                   l2_cache_blocks, &l1_result, &l2_result, 0);
	/* Check results */
  EXPECT_EQ(status, 0);
  EXPECT_EQ(l2_result.dirty_eviction, 0);
  expected_miss_hit_l1 = loadstore ? MISS_STORE : MISS_LOAD;
  EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
  expected_miss_hit_l2 = loadstore ? MISS_STORE : MISS_LOAD;
  EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
  EXPECT_EQ(status, OK);

	/* Force L1 and L2 hit */
	status = lru_replacement_policy_l1_l2(&l1_l2_info, loadstore,
																				l1_cache_blocks, l2_cache_blocks,
																				&l1_result, &l2_result, 0);
	/* Check results */
	EXPECT_EQ(status, 0);
	EXPECT_EQ(l2_result.dirty_eviction, 0);
	expected_miss_hit_l1 = loadstore ? HIT_STORE : HIT_LOAD;
	EXPECT_EQ(l1_result.miss_hit, expected_miss_hit_l1);
	expected_miss_hit_l2 = loadstore ? HIT_STORE : HIT_LOAD;
	EXPECT_EQ(l2_result.miss_hit, expected_miss_hit_l2);
	EXPECT_EQ(status, OK);
  
}


/*
 * TEST3:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 5. Se llena L1 al producir accesos distintos que A
 * 6. Se genera un miss en L1 y L2 al accesar A
 * 7. Se accesa A para producir un hit en L1 y L2
 * 8. Se verifica que hay hit en L1 y L2
 */
TEST_F(L2cache,l1_hit_l2_hit){
	DEBUG(debug_on, l1_hit_l2_hit);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;
	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Se llenan L1 y L2
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		//se deben forzar tags distintos, mismo indice (misma linea) y que no hayan repetidos
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Before insertion of block A **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}


	// Se inserta A
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** After insertion of block A, before forced hit **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Print state
	if(debug_on){
		printf("\n** After forced hit **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	int expected = (loadstore) ? HIT_STORE : HIT_LOAD;
	// Comprobación de hit en L1
	EXPECT_EQ(l1_result.miss_hit,expected);
	// Comprobación de hit en L2
	EXPECT_EQ(l2_result.miss_hit,expected);

}



/*
 * TEST4:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y hit en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_hit){
	DEBUG(debug_on, l1_miss_l2_hit);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;

	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and L2 **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se inserta A en L1 y L2
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se hacen accsesos hasta victimizar A de L1 (pero A sigue estando en L2)
	for (size_t i = 0; i < sizes.l1_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 but not from L2 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A: esto fuerza miss en L1 y Hit en L2.
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l1_result.miss_hit,expected_l1);
	// Comprobación de hit en L2
	int expected_l2 = (loadstore) ? HIT_STORE : HIT_LOAD;
	EXPECT_EQ(l2_result.miss_hit,expected_l2);
}


/*
 * TEST5:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 y L2 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_miss){
	DEBUG(debug_on, l1_miss_l2_miss);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;

	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and L2 **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se inserta A en L1 y L2
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se hacen accsesos hasta victimizar A de L1 y de L2
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(l2_cache_blocks,B.l2_assoc,B.l2_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and L2 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A: esto fuerza miss en L1 y miss en L2.
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l1_result.miss_hit,expected_l1);
	// Comprobación de hit en L2
	int expected_l2 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l2_result.miss_hit,expected_l2);
}
