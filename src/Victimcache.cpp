/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info *l1_vc_info,
    	                      	 bool loadstore,
       	                    	 entry* l1_cache_blocks,
       	                  	 entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	         bool debug)
{
    /* Define auxiliary variables */
	bool hit;
	int hit_index;
	int vc_index = 0;
    int vc_rp = 0;
    int hit_rp = 0;
    /*start working with l1*/
  	lru_replacement_policy (l1_vc_info->l1_idx, l1_vc_info->l1_tag, l1_vc_info->l1_assoc,loadstore, l1_cache_blocks,l1_result,debug);
	
    if(((l1_result->miss_hit == MISS_STORE)||(l1_result->miss_hit == MISS_LOAD))  && l1_result->evicted_address != 0){
		/* Start cache block search */
		for (int i = 0; i < l1_vc_info->vc_assoc; i++ ) {
      		
			if (vc_cache_blocks[i].valid != true){
                vc_rp =16;
				vc_index = i;
                hit = false;
				break;
			}
            /* If there's a cache block hit:
            1. Save hit index
            2. Block is replaced
            3. Set the found block's rp value to 0
            4. Hit boolean is now true */
			if (vc_cache_blocks[i].tag == l1_vc_info->l1_tag) {
				hit = true;
				hit_index = i;
                hit_rp = vc_cache_blocks[i].rp_value;
				vc_cache_blocks[i].tag = l1_result-> evicted_address;
				vc_cache_blocks[i].rp_value = 0;
				/* Update vc dirty bit and results */
				if (loadstore == 1){
					vc_result->miss_hit = HIT_STORE;
				}else{
					vc_result->miss_hit = HIT_LOAD;
				}
				break;
			}
			else {
				hit = false;
			}
            /* Find which cache block has the max rp value aka the least recently used block*/
            if (vc_cache_blocks[i].rp_value > vc_rp){  
               
                vc_rp = vc_cache_blocks[i].rp_value;
                vc_index = i;
            }
		}
        /* Start vc cache block miss operations */
        if (!hit){
            vc_result->dirty_eviction = 0;
            vc_result->evicted_address = 0;
            if (vc_cache_blocks[vc_index].valid){ 
                vc_result->dirty_eviction = vc_cache_blocks[vc_index].dirty;
                vc_result->evicted_address = vc_cache_blocks[vc_index].tag;
            }

            vc_cache_blocks[vc_index].tag = l1_result->evicted_address;
            vc_cache_blocks[vc_index].valid = true;
            vc_cache_blocks[vc_index].rp_value = 0;
        /* Update vc dirty bit and results */
            if (loadstore == 1){
                vc_cache_blocks[vc_index].dirty = true; 
                vc_result->miss_hit = MISS_STORE;
            }else{
                
                vc_result->miss_hit = MISS_LOAD;
            }
            /* Update rp value of all vc cache blocks */
            for (int i = 0; i < l1_vc_info->vc_assoc; i++){
                if (i != vc_index&& vc_cache_blocks[i].rp_value<=vc_rp&&vc_cache_blocks[i].valid){
                    vc_cache_blocks[i].rp_value += 1;
                }
            }

        }else{ 
                /* Update vc dirty bit and results */
                if (loadstore == 1){
                    vc_result->miss_hit = HIT_STORE;
                }else{
                    vc_result->miss_hit = HIT_LOAD;
                }
                /* Update rp value of all vc cache blocks */
                for (int i = 0; i < l1_vc_info->vc_assoc; i++){
                    if (i != hit_index && vc_cache_blocks[i].rp_value<=hit_rp&&vc_cache_blocks[i].valid){
                        vc_cache_blocks[i].rp_value += 1;
                    }
                }
        }
       
    }
   	return OK;
}
