#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bitset>
#include <L1cache.h>
#include <debug_utilities.h>
#include <sstream>

#define KB 1024
/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}

int main(int argc, char * argv []) {
  /* Define auxiliary variables */
  float total_IC = 0;
  float hit_rate;
  float miss_rate;
  float cache_hits = 0;
  float memory_request = 0;
  float hit_time = 1;
  float CPU_time;   
  float AMAT; 
  float miss_penalty = 20;
  float read_miss_rate = 0;
  float load_misses = 0;
  float store_misses = 0;
  float total_misses = 0;
  float load_hits = 0;
  float store_hits = 0; 
  float total_hits = 0;
  float dirty_eviction =0;
  std::string chosen_policy_s;
  /* Parse argruments */
  std::string cache_size_s = argv[2];
  std::string associativity_s = argv[4];
  std::string line_size_s = argv[6];
  std::string chosen_policy_ss = argv[8];
  int cache_size;
  std::istringstream(cache_size_s) >> cache_size;
  int associativity;
  std::istringstream(associativity_s) >> associativity;
  int line_size;
  std::istringstream(line_size_s) >> line_size;
  int chosen_policy;
  std::istringstream(chosen_policy_ss) >> chosen_policy;

  /* Get bits number for idx, tag and offset */
  struct cache_params cache_L1_params = {cache_size,associativity,line_size};
  struct cache_field_size field_size;
  field_size_get(cache_L1_params, &field_size); 

  /*Cache construc*/
  int total_blocks = cache_L1_params.size*KB/cache_L1_params.block_size;
  int total_sets = total_blocks/cache_L1_params.asociativity;
  struct entry cache_L1[total_sets][associativity];


    for(int i = 0; i <total_sets;i++){
        for(int h = 0; h <associativity;h++){
            cache_L1[i][h].valid = 0;
            if ( chosen_policy == 2 ){
                cache_L1[i][h].rp_value = 1;
            }
        }
    }
  /* Get trace's lines and start your simulation */
  std::string trace;
  while (std::getline(std::cin,trace)){
    /*Count memory request*/
    memory_request = memory_request + 1;
    int idx;
    int tag;
    struct operation_result result = {};
    int length_trace = trace.length();
    char LS_s = trace[2];
    bool LS; // LS
    LS = (LS_s == '1');
    std::string address_str = trace.substr(4,8);
    std::string IC_s = trace.substr(13);
    int IC;
    std::istringstream(IC_s) >> IC;

    total_IC = total_IC + IC;
    /*HEX adress to long int(decimal)*/
    std::stringstream str;
    str << address_str;
    long address_int; /* decimal address */
    str >> std::hex >> address_int;
    address_tag_idx_get(address_int,field_size, &idx, &tag);
    /*Determinate which policy use*/
    if ( chosen_policy == 0 ) {
        lru_replacement_policy(idx,tag,associativity,LS,cache_L1[idx],&result,0);
        chosen_policy_s = "LRU";
    }
    else if ( chosen_policy == 1 ){
        srrip_replacement_policy(idx,tag,associativity,LS,cache_L1[idx],&result,0);
        chosen_policy_s = "SRRIP";
    }
    else {
        nru_replacement_policy(idx,tag,associativity,LS,cache_L1[idx],&result,0);
        chosen_policy_s = "NRU";
    }
    /* Determinate dirty evictions*/
    if (result.dirty_eviction == 1) {
        dirty_eviction = dirty_eviction + 1;
    }
    if ( LS == 0 ) { 
        if (result.miss_hit == 0) {
        /* miss */
            load_misses = load_misses + 1;
        }
        else {
        /* hit */
            cache_hits = cache_hits + 1;   
            load_hits = load_hits + 1;
        }
    }  
    else {
        if (result.miss_hit == 1) {
        /* miss */
            store_misses = store_misses + 1;
        }
        else {
        /* hit */
            cache_hits = cache_hits + 1;   
            store_hits = store_hits + 1;
        }
    }
    }
    /*Calculate statics*/
    read_miss_rate = load_misses / (load_misses + load_hits);
    hit_rate = cache_hits / memory_request;
    miss_rate = (store_misses + load_misses )/ memory_request;
    total_misses = store_misses + load_misses;
    total_hits = store_hits + load_hits;
    CPU_time = total_IC + (total_misses * miss_penalty);
    AMAT = hit_time + (miss_rate * miss_penalty);
    /* Print cache configuration */
    std::cout << "###############################################" << std::endl;
    std::cout << "  Cache parameters                  " << std::endl;
    std::cout << "###############################################" << std::endl;
    std::cout << "Cache replacement policy:           " << chosen_policy_s << std::endl;
    std::cout << "Cache Size (KB):                    "<< cache_size << std::endl;
    std::cout << "Cache Associativity:                "<< associativity  << std::endl;
    std::cout << "Cache Block size (bytes):           "<< line_size << std::endl;
    /* Print Statistics */
    std::cout << "###############################################" << std::endl;
    std::cout << "  Simulation results"        << std::endl;
    std::cout << "###############################################" << std::endl;
    std::cout << "CPU time :                          "<< static_cast<int>(CPU_time) << std::endl;
    std::cout << "AMAT (cycles):                      "<< AMAT << std::endl;
    std::cout << "Overall miss rate:                  "<< miss_rate << std::endl;
    std::cout << "Read miss rate:                     "<< read_miss_rate << std::endl;
    std::cout << "Dirty evictions:                    "<< static_cast<int>(dirty_eviction) << std::endl;
    std::cout << "Load misses:                        "<< static_cast<int>(load_misses) << std::endl;
    std::cout << "Store misses:                       "<< static_cast<int>(store_misses) << std::endl;
    std::cout << "Total misses:                       "<< static_cast<int>(total_misses) << std::endl;
    std::cout << "Load hits:                          "<< static_cast<int>(load_hits) << std::endl;
    std::cout << "Store hits:                         "<< static_cast<int>(store_hits) << std::endl;
    std::cout << "Total hits:                         "<< static_cast<int>(total_hits) << std::endl;
    std::cout << "###############################################" << std::endl;
return 0;
}
