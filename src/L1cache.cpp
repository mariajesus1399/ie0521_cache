/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <bitset>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
	/* Calculate number of bits for each field*/
   int total_blocks = cache_params.size*KB/cache_params.block_size;
   int total_sets = total_blocks/cache_params.asociativity;
   field_size->offset = log2(cache_params.block_size);
   field_size->idx = log2(total_sets);
   field_size->tag = ADDRSIZE - field_size->offset - field_size->idx;

  return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
	 /* Get index and address */
   string binary_address = bitset<32>(address).to_string();
   string taken_bits_for_tag = binary_address.substr(0,field_size.tag);
   string taken_bits_for_idx = binary_address.substr(field_size.tag, field_size.idx);
   *tag = static_cast<int>(bitset<32>(taken_bits_for_tag).to_ulong()); //c++98
   *idx = static_cast<int>(bitset<32>(taken_bits_for_idx).to_ulong()); //c++98

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  /* Define auxiliary variables */
   bool hit = false;
   int hit_idx; 
   bool stop = false;
   int distante;
   int rrip_index = -1;

  /*Check valid parameters*/
   if (debug == 1) {
   if (idx == 0 || idx < 0){
    return PARAM;
   }
   if (tag == 0 || tag < 0){
    return PARAM;
   }
   if (associativity == 0 || associativity < 0 ){
    return PARAM;
   }
  }
  /* Define 'distante' */
   if(associativity < 3){
      distante = 1;
   }else{
      distante = 3;
   }
   while (!stop){
     /* Start L1 cache block search */
      for (int i = 0; i < associativity; i++){
         if (cache_blocks[i].valid){
            if(rrip_index == -1 && cache_blocks[i].rp_value == distante){
               rrip_index = i;
            }
            /* If there's a cache block hit:
            1. Set the found block's rp value to 0
            2. Hit boolean is now true */
            if (cache_blocks[i].tag == tag){
               hit = true;
               hit_idx = i;
               cache_blocks[i].rp_value = 0;
            }
         } else {
            rrip_index = i;
            break;
         }
      }
      /* Start L1 cache block hit operations */
      if (hit) {
        /* Update L1 dirty bit and results */
         if (loadstore == 1){
            cache_blocks[hit_idx].dirty = true;
            result->miss_hit = HIT_STORE;
         } else {
            result->miss_hit = HIT_LOAD;
         }
         stop = true;
      /* Start L1 cache block miss operations */
      } else {
         if (rrip_index == -1) {
           /* Update rp value of all L1 cache blocks */
            for (int i = 0; i < associativity; i++){
               cache_blocks[i].rp_value += 1;
            }
         }else {
            stop = true;
            result->dirty_eviction = false;
            result->evicted_address = 0;
            /* If replaced block was valid, write back to memory */
            if (cache_blocks[rrip_index].valid){
               result->dirty_eviction = cache_blocks[rrip_index].dirty;
               result->evicted_address = cache_blocks[rrip_index].tag; 
            }
            /*Replace the block*/          
            cache_blocks[rrip_index].tag = tag;
            cache_blocks[rrip_index].valid = true;
            cache_blocks[rrip_index].rp_value = distante - 1;
            /* Update L1 dirty bit and results */
            if (loadstore == 1){
               cache_blocks[rrip_index].dirty = true;
               result->miss_hit = MISS_STORE;
            }else{
               cache_blocks[rrip_index].dirty = false;
               result->miss_hit = MISS_LOAD;
            }
         }
      }

   }
    return OK;
}



int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  /*Check valid parameters*/
   if (debug == 1) {
   if (idx == 0 || idx < 0){
    return PARAM;
   }
   if (tag == 0 || tag < 0){
    return PARAM;
   }
   if (associativity == 0 || associativity < 0 ){
    return PARAM;
   }
  }
  /* Define auxiliary variables */
  bool hit = false;
  int hit_index;
  int hit_rrp_value;
  int LRU_index = 0;
  int max_rp_value = cache_blocks[0].rp_value;
	/* Start L1 cache block search */
  for (int i = 0; i < associativity; i++) {
    if (cache_blocks[i].valid != true) {
      LRU_index = i;
      break;
    }
		/* If there's a cache block hit:
      1. Save hit index
      2. Save the found block's original rp value
      3. Set the found block's rp value to 0
      4. Hit boolean is now true */
    if (cache_blocks[i].tag == tag) {
      hit_index = i;
      hit_rrp_value = cache_blocks[i].rp_value;
      cache_blocks[i].rp_value = 0;
      hit = true;
    }
    /* Find which cache block has the max rp value aka the least recently used block*/
    if (cache_blocks[i].rp_value > max_rp_value) {  
      max_rp_value = cache_blocks[i].rp_value;
      LRU_index = i;
    }
  }

	/* Start L1 cache block miss operations */
  if (!hit) {
		/* If replaced block was valid, write back to memory */
    if (cache_blocks[LRU_index].valid) {  
      result->dirty_eviction = cache_blocks[LRU_index].dirty;
      result->evicted_address = cache_blocks[LRU_index].tag;  
    }
		/* Insert new block into cache */
    cache_blocks[LRU_index].tag = tag;
    cache_blocks[LRU_index].valid = true;
    cache_blocks[LRU_index].rp_value = 0;
		/* Update L1 dirty bit and results */
    if (loadstore == 1) {
      cache_blocks[LRU_index].dirty = true;  
      result->miss_hit = MISS_STORE;
    } else {
      cache_blocks[LRU_index].dirty = false; 
      result->miss_hit = MISS_LOAD;
    }
	  /* Update rp value of all L1 cache blocks */
    for (int i = 0; i < associativity; i++) {
      if (i != LRU_index) {
        cache_blocks[i].rp_value += 1;
      }
    }

	/* Start L1 cache block hit operations */
  } else {  
		/* Update L1 dirty bit and results */
    if (loadstore == 1) {
      result->miss_hit = HIT_STORE;
			cache_blocks[hit_index].dirty = true; 
    } else {
      result->miss_hit = HIT_LOAD;
    }
		/* Update rp value of all L1 cache blocks */
    for (int i = 0; i < associativity; i++) {
      if (i != hit_index && cache_blocks[i].rp_value < hit_rrp_value) {
        cache_blocks[i].rp_value += 1;
      }
    }
  }
  return OK; 
}
int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{

  /*Check valid parameters*/
   if (debug == 1) {
   if (idx == 0 || idx < 0){
    return PARAM;
   }
   if (tag == 0 || tag < 0){
    return PARAM;
   }
   if (associativity == 0 || associativity < 0 ){
    return PARAM;
   }
  }
  /* Define auxiliary variables */
  bool FLAG = true;
  bool hit = false;
  int  hit_index;
  int NRU_index;
  /*go through all the blocks to know if it is a hit*/
  for (int i = 0; i < associativity; i++ ) {
      if (cache_blocks[i].valid != true){
        NRU_index = i;
        break;
      }
      /*Found a hit*/
      if (cache_blocks[i].tag == tag) {
	    hit_index = i;
        hit = true;
        cache_blocks[i].rp_value = 0;
        if (loadstore == 1){
           cache_blocks[i].dirty = true;
           operation_result->miss_hit = HIT_STORE;
        }else{
           operation_result->miss_hit = HIT_LOAD;
        }
        break;
      }
      else {
        hit = false;
      }
  }
  /*If isn´t a hit*/
  if (hit == false) {
    /*go through all the block to find the first 1*/
    while (FLAG) {
        for (int j = 0; j < associativity; j++ ) {
          if (cache_blocks[j].rp_value == 1) {
            /*Replace the block*/
            cache_blocks[j].rp_value = 0;
            if (cache_blocks[j].valid) { 
              operation_result->dirty_eviction = cache_blocks[j].dirty;
              operation_result->evicted_address = cache_blocks[j].tag;
            }
            cache_blocks[j].tag = tag;
            cache_blocks[j].valid = true;
            FLAG = false;
            if (loadstore == 1){
               cache_blocks[j].dirty = true;
               operation_result->miss_hit = MISS_STORE;
            }else{
               cache_blocks[j].dirty = false;
               operation_result->miss_hit = MISS_LOAD;
            }
            break;
          }
          else {
            FLAG = true;
          }
        }
        if (FLAG) {
          /*If you can´t find a 1, set all the values to 1, and repeat*/
          for (int k = 0; k < associativity; k++) {
            cache_blocks[k].rp_value = 1;
          }
        }
      }
  }
    return OK;
}
