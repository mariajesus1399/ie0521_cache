/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				operation_result* operation_result_cache_obl,
                                bool debug=false)
{
   /* Define auxiliary variables */
  bool hit = false;
  bool hit_OBL = false;
  int hit_index;
  int hit_rrp_value;
  int LRU_index = 0;
  int max_rp_value = cache_block[0].rp_value;
  int hit_index_OBL;
  int hit_rrp_value_OBL;
  int LRU_index_OBL = 0;
  int max_rp_value_OBL = cache_block_obl[0].rp_value;

  /* Start cache block and OBL block search */
  for (int i = 0; i < associativity; i++) {
    /* If cache set is not full, stop the searching to fill */
    if (cache_block[i].valid != true) {
      LRU_index = i;
      break;
    }
    /* If there's a cache block hit:
      1. Save hit index
      2. Save the found block's original rp value
      3. Set the found block's rp value to 0
      4. Hit boolean is now true */
    if (cache_block[i].tag == tag) {
      hit_index = i;
      hit_rrp_value = cache_block[i].rp_value;
      cache_block[i].rp_value = 0;
      hit = true;
    }
    /* Find which cache block has the max rp value aka the least recently used block*/
    if (cache_block[i].rp_value > max_rp_value) {  
      max_rp_value = cache_block[i].rp_value;
      LRU_index = i;
    }
    /* Repeat hit process for OBL block (B+1 block) */
    if (cache_block_obl[i].tag == tag) { 
      hit_index_OBL = i;
      hit_rrp_value_OBL = cache_block_obl[i].rp_value;
      cache_block_obl[i].rp_value = 0;
      hit_OBL = true;
    }
    /* Find which OBL block has the max rp value aka the least recently used OBL block*/
    if (cache_block_obl[i].rp_value > max_rp_value_OBL) { 
      max_rp_value_OBL = cache_block_obl[i].rp_value;
      LRU_index_OBL = i;
    }
  }

  /* Start cache block miss operations */
  if (!hit) {
    /* If replaced block was valid, write back to memory */
    if (cache_block[LRU_index].valid) {  
      operation_result_cache_block->dirty_eviction = cache_block[LRU_index].dirty;
      operation_result_cache_block->evicted_address = cache_block[LRU_index].tag;
    }
    /* Insert new block into cache */
    cache_block[LRU_index].tag = tag;
    cache_block[LRU_index].valid = true;
    cache_block[LRU_index].rp_value = 0;
    cache_block[LRU_index].obl_tag = 0;
    /* Update dirty bit and results */
    if (loadstore == 1) {
      cache_block[LRU_index].dirty = true;  
      operation_result_cache_block->miss_hit = MISS_STORE;
    } else {
      cache_block[LRU_index].dirty = false;
      operation_result_cache_block->miss_hit = MISS_LOAD;
    }
    /* Update rp value of all cache blocks */
    for (int i = 0; i < associativity; i++) {
      if (i != LRU_index) {
        cache_block[i].rp_value += 1;
      }
    }
    /* If next block was not in cache, bring it into cache with it's OBL tag set to 1*/
    if (!hit_OBL) {                            
      cache_block_obl[LRU_index_OBL].tag = tag;  
      cache_block_obl[LRU_index_OBL].valid = true;
      cache_block_obl[LRU_index_OBL].rp_value = 0;
      cache_block_obl[LRU_index_OBL].obl_tag = 1;
      /* Update dirty bit and results */
      if (loadstore == 1) {
        cache_block[LRU_index_OBL].dirty = true;  
        operation_result_cache_obl->miss_hit = MISS_STORE;
      } else {
        cache_block[LRU_index_OBL].dirty = false;
        operation_result_cache_obl->miss_hit = MISS_LOAD;
      }
      /* Update rp value of all OBL blocks */
      for (int i = 0; i < associativity; i++) {
        if (i != LRU_index_OBL) {
          cache_block_obl[i].rp_value += 1;
        }
      }
    /* If next block was in cache, just update results*/
    } else {  
      if (loadstore == 1) {
        operation_result_cache_obl->miss_hit = HIT_STORE;
      } else {
        operation_result_cache_obl->miss_hit = HIT_LOAD;
      }
    }

  /* Start cache block hit operations */
  } else {  
    /* Update dirty bit and results */
    if (loadstore == 1) {
      operation_result_cache_block->miss_hit = HIT_STORE;
      cache_block[hit_index].dirty = true;
    } else {
      operation_result_cache_block->miss_hit = HIT_LOAD;
    }
    /* Update rp value of all cache blocks */
    for (int i = 0; i < associativity; i++) {
      if (i != hit_index && cache_block[i].rp_value < hit_rrp_value) {
        cache_block[i].rp_value += 1;
      }
    }
    /* If cache block's OBL tag was set to 1, update it's tag and prefetch next block */
    if (cache_block[hit_index].obl_tag) {
      cache_block[hit_index].obl_tag = 0;
      /* If next block was not in cache, bring it into cache with it's OBL tag set to 1*/
      if (!hit_OBL) {                            
        cache_block_obl[LRU_index_OBL].tag = tag;  
        cache_block_obl[LRU_index_OBL].valid = true;
        cache_block_obl[LRU_index_OBL].rp_value = 0;
        cache_block_obl[LRU_index_OBL].obl_tag = 1;
        /* Update dirty bit and results */
        if (loadstore == 1) {
          cache_block[LRU_index_OBL].dirty = true;  
          operation_result_cache_obl->miss_hit = MISS_STORE;
        } else {
          cache_block[LRU_index_OBL].dirty = false;
          operation_result_cache_obl->miss_hit = MISS_LOAD;
        }
        /* Update rp value of all OBL blocks */
        for (int i = 0; i < associativity; i++) {
          if (i != LRU_index_OBL) {
            cache_block_obl[i].rp_value += 1;
          }
        }
      /* If next block was in cache, just update results*/
      } else {  
        if (loadstore == 1) {
          operation_result_cache_obl->miss_hit = HIT_STORE;
        } else {
          operation_result_cache_obl->miss_hit = HIT_LOAD;
        }
      }
    /* If cache block's OBL tag was set to 0, no aditional prefetch is done */
    } else {
      /* If next block was not in cache, just update results and dirty bit*/ 
      if (!hit_OBL) {  
        if (loadstore == 1) {
          cache_block[LRU_index_OBL].dirty = true;  
          operation_result_cache_obl->miss_hit = MISS_STORE;
        } else {
          cache_block[LRU_index_OBL].dirty = false;
          operation_result_cache_obl->miss_hit = MISS_LOAD;
        }
      /* If next block was in cache, just update results*/ 
      } else {  
        if (loadstore == 1) {
          operation_result_cache_obl->miss_hit = HIT_STORE;
        } else {
          operation_result_cache_obl->miss_hit = HIT_LOAD;
        }
      }
    }
  }

  return OK;
}
