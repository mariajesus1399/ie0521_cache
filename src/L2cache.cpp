/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int l1_l2_entry_info_get(const struct cache_params l1_params,
                         const struct cache_params l2_params, long address,
                         entry_info* l1_l2_info, bool debug) {
  /* Define auxiliary variables */
  struct cache_field_size field_size_l1;
  struct cache_field_size field_size_l2;
  int idx_l1;
  int idx_l2;
  int tag_l1;
  int tag_l2;
  /* Call functions to construct cache lines and get adresses */
  field_size_get(l1_params, &field_size_l1);
  field_size_get(l2_params, &field_size_l2);
  address_tag_idx_get(address, field_size_l1, &(idx_l1), &(tag_l1));
  address_tag_idx_get(address, field_size_l2, &(idx_l2), &(tag_l2));
  /* Pass parameters */
  l1_l2_info->l1_assoc = l1_params.asociativity;
  l1_l2_info->l2_assoc = l2_params.asociativity;
  l1_l2_info->l1_idx = (u_int)idx_l1;
  l1_l2_info->l2_idx = (u_int)idx_l2;
  l1_l2_info->l1_tag = (u_int)tag_l1;
  l1_l2_info->l2_tag = (u_int)tag_l2;
}

int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug) 
{
	/* Define auxiliary variables */
  bool hit_l1 = false;
  bool hit_l2 = false;
  int hit_index_l1;
  int hit_index_l2;
  int hit_rrp_value_l1;
  int hit_rrp_value_l2;
  int LRU_index_l1 = 0;
  int LRU_index_l2 = 0;
  int max_rp_value_l1 = l1_cache_blocks[0].rp_value;
  int max_rp_value_l2 = l2_cache_blocks[0].rp_value;
  /* Start L1 cache block search */
  for (int i = 0; i < l1_l2_info->l1_assoc; i++) {
    /* If cache set is not full, stop the searching to fill */
    if (l1_cache_blocks[i].valid != true) {
      LRU_index_l1 = i;
      break;
    }
    /* If there's a cache block hit:
		1. Save hit index
		2. Save the found block's original rp value
		3. Set the found block's rp value to 0
		4. Hit boolean is now true */
    if (l1_cache_blocks[i].tag == l1_l2_info->l1_tag) {
      hit_index_l1 = i;
      hit_rrp_value_l1 = l1_cache_blocks[i].rp_value;
      l1_cache_blocks[i].rp_value = 0;
      hit_l1 = true;
    }
    /* Find which cache block has the max rp value aka the least recently used block*/
    if (l1_cache_blocks[i].rp_value > max_rp_value_l1) {
      max_rp_value_l1 = l1_cache_blocks[i].rp_value;
      LRU_index_l1 = i;
    }
  }
  /* Start L2 cache block search */
  for (int i = 0; i < l1_l2_info->l2_assoc; i++) {
    if (l2_cache_blocks[i].valid != true) {
      LRU_index_l2 = i;
      break;
    }
    /* Repeat hit process for L2 block */
    if (l2_cache_blocks[i].tag == l1_l2_info->l2_tag) {
      hit_index_l2 = i;
      hit_rrp_value_l2 = l2_cache_blocks[i].rp_value;
      l2_cache_blocks[i].rp_value = 0;
      hit_l2 = true;
    }
    /* Find which cache block has the max rp value aka the least recently used block*/
    if (l2_cache_blocks[i].rp_value > max_rp_value_l2) {
      max_rp_value_l2 = l2_cache_blocks[i].rp_value;
      LRU_index_l2 = i;
    }
  }

  /* Start L1 (and therefore L2) cache block hit operations */
  if (hit_l1) {
    /* Update rp value of all L1 cache blocks */
    for (int i = 0; i < l1_l2_info->l1_assoc; i++) {
      if (i != hit_index_l1 && l1_cache_blocks[i].rp_value < hit_rrp_value_l1) {
        l1_cache_blocks[i].rp_value += 1;
      }
    }
    /* Update rp value of all L2 cache blocks */
    for (int i = 0; i < l1_l2_info->l2_assoc; i++) {
      if (i != hit_index_l2 && l2_cache_blocks[i].rp_value < hit_rrp_value_l2) {
        l2_cache_blocks[i].rp_value += 1;
      }
    }
    /* Update L1 and L2 dirty bit and results */
    if (loadstore == 1) {
			l2_cache_blocks[hit_index_l2].dirty = true;
      l1_result->miss_hit = HIT_STORE;
      l2_result->miss_hit = HIT_STORE;
    } else {
      l1_result->miss_hit = HIT_LOAD;
      l2_result->miss_hit = HIT_LOAD;
    }

    /* Start L1 cache block miss operations */
  } else {
    /* Start just L2 cache block hit operations */
    if (hit_l2) {
      /* If replaced block was valid, write back to memory */
      if (l1_cache_blocks[LRU_index_l1].valid) {
        l1_result->dirty_eviction = l1_cache_blocks[LRU_index_l1].dirty;
        l1_result->evicted_address = l1_cache_blocks[LRU_index_l1].tag;
      }
      /* Insert new block into L1 cache */
      l1_cache_blocks[LRU_index_l1].tag = l1_l2_info->l1_tag;
      l1_cache_blocks[LRU_index_l1].valid = true;
      l1_cache_blocks[LRU_index_l1].rp_value = 0;
      /* Update L1 and L2 dirty bit and results */
      if (loadstore == 1) {
        l2_cache_blocks[hit_index_l2].dirty = true; 
        l1_result->miss_hit = MISS_STORE;
        l2_result->miss_hit = HIT_STORE;
      } else {
        l1_result->miss_hit = MISS_LOAD;
        l2_result->miss_hit = HIT_LOAD;
      }
      /* Update rp value of all L1 cache blocks */
      for (int i = 0; i < l1_l2_info->l1_assoc; i++) {
        if (i != LRU_index_l1) {
          l1_cache_blocks[i].rp_value += 1;
        }
      }
      /* Update rp value of all L2 cache blocks */
      for (int i = 0; i < l1_l2_info->l2_assoc; i++) {
        if (i != hit_index_l2 &&
            l2_cache_blocks[i].rp_value < hit_rrp_value_l2) {
          l2_cache_blocks[i].rp_value += 1;
        }
      }
    /* Start L2 and L1 cache block miss operations */
    } else {
      /* If replaced block was valid, write back to memory */
      if (l2_cache_blocks[LRU_index_l2].valid) {
        l2_result->dirty_eviction = l2_cache_blocks[LRU_index_l2].dirty;
        l2_result->evicted_address = l2_cache_blocks[LRU_index_l2].tag;
      }
      /* Insert new block into cache */
      l1_cache_blocks[LRU_index_l1].tag = l1_l2_info->l1_tag;
      l1_cache_blocks[LRU_index_l1].valid = true;
      l1_cache_blocks[LRU_index_l1].rp_value = 0;
      l2_cache_blocks[LRU_index_l2].tag = l1_l2_info->l2_tag;
      l2_cache_blocks[LRU_index_l2].valid = true;
      l2_cache_blocks[LRU_index_l2].rp_value = 0;
      /* Update L1 and L2 dirty bit and results */
      if (loadstore == 1) {
        l2_cache_blocks[LRU_index_l2].dirty = true;  
        l1_result->miss_hit = MISS_STORE;
        l2_result->miss_hit = MISS_STORE;
      } else {
        l2_cache_blocks[LRU_index_l2].dirty = false;
        l1_result->miss_hit = MISS_LOAD;
        l2_result->miss_hit = MISS_LOAD;
      }
      /* Update rp value of all L1 cache blocks */
      for (int i = 0; i < l1_l2_info->l1_assoc; i++) {
        if (i != LRU_index_l1) {
          l1_cache_blocks[i].rp_value += 1;
        }
      } /* Update rp value of all L2 cache blocks */
      for (int i = 0; i < l1_l2_info->l2_assoc; i++) {
        if (i != LRU_index_l2) {
          l2_cache_blocks[i].rp_value += 1;
        }
      }
    }
  }
  return OK;
}
